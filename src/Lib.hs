module Lib
    ( runMinesweeper
    ) where

import qualified Data.Set as S
import Data.Function
import Parser
import Types

indToPoint (GridSize w _) i = Point x y
  where
    y = floor ((fromIntegral i) / (fromIntegral w))
    x = i - (w * y)

findMines :: [(Point,Cell)] -> GridSize -> S.Set Point
findMines cells s =
  cells
  & filter (\(_,c) -> c == Mine)
  & map fst
  & S.fromList

neighbours :: Point -> GridSize -> [Point]
neighbours (Point x y) s =
  [Point (x+x') (y+y')
    | x' <- [-1..1]
    , y' <- [-1..1]
    , not (originP x' y')]
  & filter (validPoint s)
  where
    originP x y = x == 0 && y == 0
    validPoint (GridSize w h) (Point x y)
      | x < 0 || y < 0 || x >= w || y >= h = False
      | otherwise = True

countMines :: [(Point,Cell)] -> S.Set Point -> GridSize -> [Char]
countMines cells mines s = map count cells
  where
    count (p,c)
      | c == Mine = '*'
      | otherwise = head $ show $ countAtPoint mines p s
    countAtPoint mines cellPoint s =
      length $ filter (\n -> S.member n mines) $ neighbours cellPoint s

printResult :: [Char] -> GridSize -> IO ()
printResult [] _ = do return ()
printResult counts s@(GridSize w _) = do
  let (row,rest) = splitAt w counts
  putStrLn row
  printResult rest s

processField :: Field -> IO ()
processField (Field s cs) = do printResult result s
  where
    cells = cs & zip [0..] & map (\(i,c) -> ((indToPoint s i), c))
    mines = findMines cells s
    result = countMines cells mines s

processFields [] _ = return ()
processFields (f:fs) n = do
  putStrLn $ "\nField #" ++ show n ++ ":"
  processField f
  processFields fs (n + 1)

runMinesweeper :: String -> IO ()
runMinesweeper input = do
  result <- parseFile input
  case result of
    Left er -> putStrLn $ show er
    Right fs -> processFields fs 1
