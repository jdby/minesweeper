module Parser
    ( parseFile
    ) where

import Types
import Text.ParserCombinators.Parsec (ParseError)
import Text.Parsec
import Text.Parsec.String (Parser, parseFromFile)

parseMine :: Parser Cell
parseMine = char '*' >> return Mine

parseEmpty :: Parser Cell
parseEmpty = char '.' >> return Space

parseCell :: Parser Cell
parseCell = parseMine <|> parseEmpty

spacesThenEndOfLine :: Parser ()
spacesThenEndOfLine = many (char ' ') >> endOfLine >> return ()

row :: Int -> Parser [Cell]
row n = do
  cells <- count n parseCell
  spacesThenEndOfLine
  return cells

cells :: Int -> Int ->  Parser [Cell]
cells _ 0 = return []
cells w h = do
  row <- row w
  rest <- cells w (h - 1)
  return $ concat [row, rest]

int :: Parser Int
int = do
  s <- many1 digit
  return $ read s

field :: Parser Field
field = do
  n <- int -- rows
  spaces
  m <- int -- columns
  spaces
  cells <- cells m n 
  return $ Field (GridSize m n) cells

fields :: Parser [Field]
fields = do
  games <- many field
  return games

parseFile :: String -> IO (Either ParseError [Field])
parseFile = parseFromFile fields
