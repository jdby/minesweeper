module Types where

data Cell = Mine | Space
          deriving (Show, Eq)

data Field = Field GridSize [Cell]
          deriving Show

data Point = Point { coordX :: Int, coordY :: Int }
           deriving (Show, Eq, Ord)

data GridSize = GridSize { width :: Int, height :: Int }
              deriving Show

