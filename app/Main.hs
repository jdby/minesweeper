module Main where

import System.Environment (getArgs)
import System.IO
import Lib

main :: IO ()
main = do
  args <- getArgs
  runMinesweeper (args !! 0)
